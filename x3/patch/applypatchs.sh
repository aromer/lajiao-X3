echo "Obtaining build directory..."
rootdirectory="$PWD"
cd $rootdirectory
cd frameworks/base
git am $rootdirectory/device/lajiao/x3/patch/base/*.patch
cd $rootdirectory
cd packages/apps/DeskClock
git am $rootdirectory/device/lajiao/x3/patch/DeskClock/*.patch
cd $rootdirectory
cd packages/apps
rm -rf VideoEditor
#rm -rf SoundRecorder
rm -rf SpeechRecorder
#rm -rf CMFileManager
rm -rf VoiceDialer
cd $rootdirectory
cd packages/inputmethods
rm -rf LatinIME
cd $rootdirectory
