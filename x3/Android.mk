ifneq ($(filter x3,$(TARGET_DEVICE)),)

LOCAL_PATH := $(call my-dir)
include device/lajiao/x3/kernel/AndroidKernel.mk
include $(call all-makefiles-under,$(LOCAL_PATH))

# Create a link for the WCNSS config file, which ends up as a writable
# version in /data/misc/wifi
$(shell mkdir -p $(TARGET_OUT)/etc/firmware/wlan/prima; \
	mkdir -p $(TARGET_OUT)/lib/modules; \
    ln -sf /data/misc/wifi/WCNSS_qcom_cfg.ini \
	    $(TARGET_OUT)/etc/firmware/wlan/prima/WCNSS_qcom_cfg.ini; \
    ln -sf /persist/wlan_mac.bin \
	    $(TARGET_OUT)/etc/firmware/wlan/prima/wlan_mac.bin; \
    ln -sf /system/lib/modules/pronto/pronto_wlan.ko \
        $(TARGET_OUT)/lib/modules/wlan.ko)
endif
