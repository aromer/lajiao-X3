# Inherit from hardware-specific part of the product configuration
$(call inherit-product, device/lajiao/x3/full_x3.mk)

# Boot animation
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

TARGET_BOOTANIMATION_NAME := 720

# Enhanced NFC
$(call inherit-product, vendor/mk/config/nfc_enhanced.mk)

# Inherit some common Mokee stuff.
$(call inherit-product, vendor/mk/config/common_full_phone.mk)



## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := x3
PRODUCT_NAME := mk_x3
PRODUCT_BRAND := lajiao
PRODUCT_MODEL := x3
PRODUCT_MANUFACTURER := lajiao

PRODUCT_DEFAULT_LANGUAGE := zh
PRODUCT_DEFAULT_REGION := CN

#PRODUCT_LOCALES := zh_CN zh_TW en_US

