# Inherit from hardware-specific part of the product configuration
$(call inherit-product, device/xiaomi/hm2014811/full_hm2014811.mk)

# Boot animation
TARGET_SCREEN_HEIGHT := 1280
TARGET_SCREEN_WIDTH := 720

TARGET_BOOTANIMATION_NAME := 720

# Enhanced NFC
$(call inherit-product, vendor/cm/config/nfc_enhanced.mk)

# Inherit some common Mokee stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)



## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := hm2014811
PRODUCT_NAME := cm_hm2014811
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := HM2014811
PRODUCT_MANUFACTURER := Xiaomi

PRODUCT_DEFAULT_LANGUAGE := zh
PRODUCT_DEFAULT_REGION := CN

#PRODUCT_LOCALES := zh_CN zh_TW en_US

PRODUCT_PROPERTY_OVERRIDES += \
    ro.product.model=HM2014811 \
    ro.product.device=HM2014811 \
    ro.product.name=HM2014811

