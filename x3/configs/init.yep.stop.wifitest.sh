#!/system/bin/sh

LOG_TAG="wifitest"
LOG_NAME="${0}:"

hciattach_pid=""

loge ()
{
  /system/bin/log -t $LOG_TAG -p e "$LOG_NAME $@"
}

logi ()
{
  /system/bin/log -t $LOG_TAG -p i "$LOG_NAME $@"
}
logi "============================================="
logi "copy WCNSS_qcom_cfg.ini to /system/etc/wifi/..."
cp -f /system/etc/wifi/WCNSS_qcom_cfg_system.ini /system/etc/wifi/WCNSS_qcom_cfg.ini
cp -f /system/etc/wifi/WCNSS_qcom_cfg_system.ini /data/misc/wifi/WCNSS_qcom_cfg.ini
logi "ok,reboot..."
sleep(2)